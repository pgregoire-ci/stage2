#!/bin/sh
#
# usage: $0 os arch release outfile
#
set -eu

PROGBASE=$(d=$(dirname -- "${0}"); cd "${d}" && pwd)

exec /bin/sh "${PROGBASE}/${1}.sh" "${2}" "${3}" "${4}"
