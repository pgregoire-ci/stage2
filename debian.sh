#!/bin/sh
#
# Builds a Debian system capable of connecting to a network
# in order to pursue installation via ansible over SSH.
#
# usage: $0 arch release outfile
#
set -eu

ARCH="${1}"
RELEASE="${2}"
OUTFILE="${3}"


tmpdir=$(mktemp -d)

chmod 0755 "${tmpdir}"
debootstrap --variant=minbase --arch="${ARCH}" "${RELEASE}" "${tmpdir}"

mount -t proc none "${tmpdir}/proc"
mount --rbind /dev "${tmpdir}/dev"
mount --rbind /sys "${tmpdir}/sys"
mount --make-rslave "${tmpdir}/dev"
mount --make-rslave "${tmpdir}/sys"


cat >"${tmpdir}/root/stage2.sh"<<__EOF__
#!/bin/sh
set -eu

DEBIAN_FRONTEND=noninteractive
export DEBIAN_FRONTEND

printf '#!/bin/sh\\nexit 101\\n' >/usr/sbin/policy-rc.d
chmod 0555 /usr/sbin/policy-rc.d

apt-get update
apt-get -y install \\
	curl \\
	dosfstools \\
	gdisk \\
	grub-efi-${ARCH}-bin \\
	ifupdown \\
	initramfs-tools \\
	isc-dhcp-client \\
	linux-image-${ARCH} \\
	nvi \\
	openssh-server \\
	sudo \\
	systemd \\
	systemd-sysv \\
	python3-minimal

# cleanup
apt-get clean
rm -rf /var/lib/apt/lists/*
rm -f /etc/machine-id /etc/ssh/ssh_host_*

printf 'debian' >/etc/hostname
__EOF__

cat >"${tmpdir}/root/enter.sh"<<__EOF__
#!/bin/sh
set -eu

ROOT=\$(d=\$(dirname -- "\${0}"); cd "\${d}/.." && pwd)

cleanup() {
	mount | cut -d' ' -f3 | egrep "^\${ROOT}/(dev|proc|sys)" | sort -r | xargs -n1 umount
}
trap cleanup EXIT INT QUIT TERM

mount -t proc none "\${ROOT}/proc"
mount --rbind /dev "\${ROOT}/dev"
mount --rbind /sys "\${ROOT}/sys"
mount --make-rslave "\${ROOT}/dev"
mount --make-rslave "\${ROOT}/sys"
chroot "\${ROOT}"
__EOF__

chroot "${tmpdir}" /bin/sh /root/stage2.sh

(cd "${tmpdir}" && tar -f- -pv --exclude=proc/* --exclude=sys/* --exclude=dev/* --exclude=tmp/* -c *) | gzip -9 >"${OUTFILE}"
